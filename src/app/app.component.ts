import { Component, OnInit, Input } from '@angular/core';
import { setTheme } from 'ngx-bootstrap/utils';
import { NetworkService } from '../app/network.service';
import { StylesProperty } from './style.model';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  Offers: any = [];
  SubscriptionStyles: StylesProperty;
  isSubscriptionViewOpened: boolean;
  OfferID: any;

  constructor(private networkService: NetworkService, private toastr: ToastrService) {
    setTheme('bs3');
  }

  ngOnInit() {
    this.getOfferData();
  }

  @Input() set styles(style: StylesProperty) {
    this.SubscriptionStyles = style;
  }

  getOfferData() {
    const url = 'https://selfcare-service.demo.melita.com/interview/api/offers';
    this.networkService.getHttpRequestWithotHeader(url).subscribe(success => {
      console.log('Success', success);
      this.Offers = success;
      console.log('Success', this.Offers.offers[0]);
    }, error => {
      this.toastr.error('Error!', 'Please try again');
    });

  }

  OpenSubscriptions(id: any) {
    this.OfferID = id;
    this.SubscriptionStyles = {
      height: '100%',
    };
    setTimeout(() => {
      this.isSubscriptionViewOpened = true;
    }, 500);
  }
}
