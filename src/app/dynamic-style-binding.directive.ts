import { Directive, Renderer2, ElementRef, Input } from '@angular/core';
import { StylesProperty } from './style.model';

@Directive({
    selector: '[appDynamicStyleBinding]'
})
export class DynamicStyleBindingDirective {

    @Input() set styleProperties(properties: StylesProperty) {
        if (properties) {
            this.setStyleToElement(properties);
        }
    }

    constructor(private renderer: Renderer2, private element: ElementRef) {}

    /**
     *Set style to element
     *
     * @param {[StylesProperty]} styleProperties styles array
     * @memberof DynamicStyleBindingDirective
     */
    setStyleToElement(styleProperties: StylesProperty) {
        // tslint:disable-next-line: forin
        for (const style in styleProperties) {
            setTimeout(() => {
                console.log('DynamicStyleBindingDirective key : ', style , 'value: ', styleProperties[style]);
                this.renderer.setStyle(this.element.nativeElement, style, styleProperties[style]);
            }, 200);
        }
    }

}
