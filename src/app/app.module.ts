import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppComponent } from './app.component';
import { NetworkService } from '../app/network.service';
import { HttpClientModule } from '@angular/common/http';
import { SubscriptionViewComponentComponent } from './subscription-view-component/subscription-view-component.component';
import { DynamicStyleBindingDirective } from './dynamic-style-binding.directive';
import { ToastrModule } from 'ngx-toastr';

@NgModule({
  declarations: [
    AppComponent,
    SubscriptionViewComponentComponent,
    DynamicStyleBindingDirective
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot()
  ],
  providers: [
    NetworkService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
