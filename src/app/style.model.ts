export interface StylesProperty {
    width?: string;
    marginLeft?: string;
    boxShadow?: string;
    transition?: string;
    visibility?: string;
    height?: string;
    padding?: string;
};
