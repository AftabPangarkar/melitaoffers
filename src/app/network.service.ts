import { Injectable, EventEmitter } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs/internal/Observable';

@Injectable()
export class NetworkService {
    url: string;
    param: any;
    constructor(public http: HttpClient) {
    }


    getHttpRequestWithotHeader(url, param?) {
        const params = new HttpParams({
            fromObject: param
        });
        // tslint:disable-next-line: object-literal-shorthand
        return this.http.get(url, { params: params });
    }
}
