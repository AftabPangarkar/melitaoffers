import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SubscriptionViewComponentComponent } from './subscription-view-component.component';

describe('SubscriptionViewComponentComponent', () => {
  let component: SubscriptionViewComponentComponent;
  let fixture: ComponentFixture<SubscriptionViewComponentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SubscriptionViewComponentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SubscriptionViewComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
