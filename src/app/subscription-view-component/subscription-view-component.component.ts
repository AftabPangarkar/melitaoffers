import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { StylesProperty } from '../style.model';
import { NetworkService } from '../network.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-subscription-view-component',
  templateUrl: './subscription-view-component.component.html',
  styleUrls: ['./subscription-view-component.component.css']
})
export class SubscriptionViewComponentComponent implements OnInit {
  SubscriptionStyles: StylesProperty;
  subscriptions: any = [];
  @Output() isBottomSheetClosed = new EventEmitter<any>();
  @Input() OfferID: any;
  @Input() set styles(style: StylesProperty) {
    console.log('DynamicStyleBindingDirective  style: ', style);
    this.SubscriptionStyles = style;
  }

  constructor(private networkService: NetworkService, private toastr: ToastrService) { }

  ngOnInit() {
    this.getSubscriptionData();
  }

  getSubscriptionData() {

    const url = 'https://selfcare-service.demo.melita.com/interview/api/offers/' + this.OfferID + '/subscriptions';
    this.networkService.getHttpRequestWithotHeader(url).subscribe(success => {
      console.log('Success', success);
      this.subscriptions = success;
      console.log('Success', this.subscriptions.subscriptions[0]);
    }, error => {
      this.toastr.error('Error!', 'Please try again');
    });

  }

  CloseBottomSheet() {
    this.SubscriptionStyles = {
      height: '0%',
    };
    setTimeout(() => {
      this.isBottomSheetClosed.emit(true);
    }, 500);
  }


}
